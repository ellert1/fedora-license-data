# This file is for collecting the actual text found in Fedora
# packages that have used the Callaway short name, "Public Domain" 
# or the SPDX id, "LicenseRef-Fedora-Public-Domain"
# As per the instructions at https://docs.fedoraproject.org/en-US/legal/update-existing-packages/#_callaway_short_name_categories
# 
# This file is for collecting the text of such public domain dedications.
#
# Include the following information:
# 
# Fedora package name
#
# Location of where you found the public domain dedication text.
# Preferably this would be a direct link to a file. If that is not possible,
# provide enough information such that someone else can find the text in the wild
# where you found it.
#
# The actual text of the public dedication found that corresponds to the use of 
# the "Public Domain" (previously) or "LicenseRef-Fedora-Public-Domain" SPDX id.
# Remove blank lines.
#
# Copy template below and add yours to top of list, adding a space between entries.

package = 
location = 
text = '''
text here
'''

package = imagej
location = https://imagej.nih.gov/ij/disclaimer.html
text = '''
ImageJ was developed at the National Institutes of Health by an employee of the Federal Government in the course of his official duties. Pursuant to Title 17, Section 105 of the United States Code, this software is not subject to copyright protection and is in the public domain. ImageJ is an experimental system. NIH assumes no responsibility whatsoever for its use by other parties, and makes no guarantees, expressed or implied, about its quality, reliability, or any other characteristic.
'''

package = gdouros-aegean-fonts
        gdouros-aegyptus-fonts
        gdouros-akkadian-fonts
        gdouros-alexander-fonts
        gdouros-anaktoria-fonts
        gdouros-analecta-fonts
        gdouros-aroania-fonts
        gdouros-asea-fonts
        gdouros-avdira-fonts
        gdouros-musica-fonts
        gdouros-symbola-fonts
location = https://web.archive.org/web/20150625020428/http://users.teilar.gr/~g1951d/
text = '''
in lieu of a licence
Fonts and documents in this site are not pieces of property or merchandise items; they carry no trademark, copyright, license or other market tags; they are free for any use.
'''


package = perl-IO-HTML
location = https://metacpan.org/release/CJM/IO-HTML-1.004/source/examples/detect-encoding.pl
text = '''
This example is hereby placed in the public domain.
You may copy from it freely.

Detect the encoding of files given on the command line
'''

package = dotnet7.0
location = https://github.com/dotnet/fsharp/blob/186d3f6ce267c27f8acaa2c8f3df1472bf2a88bf/src/FSharp.Core/resumable.fs#L2-L3
text = '''
To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights
to this software to the public domain worldwide. This software is distributed without any warranty.
'''

package = dotnet7.0
location = https://github.com/dotnet/runtime/blob/e721ae953c61297aec73e8051696f2b9b30148c7/src/coreclr/inc/utilcode.h#L2417-L2425
text = '''
You can use this free for any purpose.  It's in the public domain.  It has no warranty.
'''

package = x11vnc
location = https://github.com/LibVNC/x11vnc/tree/af63109a17f1b1ec8b1e332d215501f11c4a33a0/misc/turbovnc
text = '''
This work has been (or is hereby) released into the public domain by
its author, Karl J. Runge <runge@karlrunge.com>. This applies worldwide.
In case this is not legally possible: Karl J. Runge grants anyone the
right to use this work for any purpose, without any conditions, unless
such conditions are required by law.
'''

package = xmlpull
location = https://github.com/xmlpull-xpp3/xmlpull-xpp3/blob/master/xmlpull/LICENSE.txt
text = '''
All of the XMLPULL API source code, compiled code, and documentation 
contained in this distribution *except* for tests (see separate LICENSE_TESTS.txt)
are in the Public Domain.
XMLPULL API comes with NO WARRANTY or guarantee of fitness for any purpose.
'''

package = xz.java
location = https://git.tukaani.org/?p=xz-java.git;a=blob;f=COPYING;h=8dd17645c4610c3d5eed9bcdd2699ecfac00406b;hb=HEAD
text = ''' 
Licensing of XZ for Java
========================
All the files in this package have been written by Lasse Collin,
Igor Pavlov, and/or Brett Okken. All these files have been put into
the public domain. You can do whatever you want with these files.
This software is provided "as is", without any warranty.
'''

package = libselinux
location = https://github.com/SELinuxProject/selinux/blob/master/libselinux/LICENSE
text = '''
This library (libselinux) is public domain software, i.e. not copyrighted.

Warranty Exclusion
------------------
You agree that this software is a
non-commercially developed program that may contain "bugs" (as that
term is used in the industry) and that it may not function as intended.
The software is licensed "as is". NSA makes no, and hereby expressly
disclaims all, warranties, express, implied, statutory, or otherwise
with respect to the software, including noninfringement and the implied
warranties of merchantability and fitness for a particular purpose.

Limitation of Liability
-----------------------
In no event will NSA be liable for any damages, including loss of data,
lost profits, cost of cover, or other special, incidental,
consequential, direct or indirect damages arising from the software or
the use thereof, however caused and on any theory of liability. This
limitation will apply even if NSA has been advised of the possibility
of such damage. You acknowledge that this is a reasonable allocation of
risk.
'''
